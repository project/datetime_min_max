(function ($, Drupal) {
  Drupal.behaviors.dateTimePickerMinMax = {
    attach: function attach(context, settings) {
      var $context = $(context);

      $context.find('input[type="text"][data-drupal-date-format]').once('datePicker').each(function () {
        let $input = $(this);
        let datepickerSettings = {};
        let dateFormat = $input.data('drupalDateFormat');

        datepickerSettings.dateFormat = dateFormat.replace('Y', 'yy').replace('m', 'mm').replace('d', 'dd');
        let maskOptions = {clearIncomplete: true};
        if ($input.attr('min')) {
          datepickerSettings.minDate = $input.attr('min');
          maskOptions.min = datepickerSettings.minDate;
        }
        if ($input.attr('max')) {
          datepickerSettings.maxDate = $input.attr('max');
          maskOptions.max = datepickerSettings.maxDate;
        }

        if ($input.attr('data-drupal-date-regional')) {
          let regional = $input.attr('data-drupal-date-regional');
          $.datepicker.setDefaults($.datepicker.regional[regional]);
        }
        $input.datepicker(datepickerSettings);
        maskOptions.inputFormat = dateFormat.replace('Y', 'yyyy').replace('m', 'mm').replace('d', 'dd');
        $input.inputmask('datetime', maskOptions);
      });
    },
    detach: function detach(context, settings, trigger) {
      if (trigger === 'unload') {
        $(context).find('input[type="text"][data-drupal-date-format]').findOnce('datePicker').datepicker('destroy');
      }
    }
  };
})(jQuery, Drupal);

<?php

namespace Drupal\datetime_min_max\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Date as CoreDate;

/**
 * Provides a form element for date selection.
 */
class Date extends CoreDate {

  /**
   * {@inheritdoc}
   */
  public static function processDate(&$element, FormStateInterface $form_state, &$complete_form) {
    if ($element['#attributes']['type'] === 'text' && !empty($element['#date_date_format'])) {
      $element['#attached']['library'][] = 'datetime_min_max/date';
      $element['#attributes']['data-drupal-date-format'] = [$element['#date_date_format']];
    }
    return $element;
  }

}

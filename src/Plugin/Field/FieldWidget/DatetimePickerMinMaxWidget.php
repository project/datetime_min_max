<?php

namespace Drupal\datetime_min_max\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'datetime_default' widget.
 *
 * @FieldWidget(
 *   id = "datetime_picker_min_max",
 *   label = @Translation("Date picker and time with min max restriction"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DatetimePickerMinMaxWidget extends DateTimeMinMaxWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $settings = $this->getSettings();
    $element['value']['#date_date_element'] = 'text';
    $element['value']['#date_date_format'] = $settings['date_date_format'];

    $min = $settings['min']['input_value'];
    if ($settings['min']['input_type'] === 'relative') {
      $min = $this->getRelativeDate($min, $settings['min']['input_modifier'], $settings['date_date_format']);
    }

    $max = $settings['max']['input_value'];
    if ($settings['max']['input_type'] === 'relative') {
      $max = $this->getRelativeDate($max, $settings['max']['input_modifier'], $settings['date_date_format']);
    }

    if (!empty($min)) {
      $element['value']['#attributes']['min'] = $min;
    }
    else {
      unset($element['value']['#attributes']['min']);
    }

    if (!empty($max)) {
      $element['value']['#attributes']['max'] = $max;
    }
    else {
      unset($element['value']['#attributes']['max']);
    }

    $element['value']['#attributes']['data-drupal-date-regional'] = \Drupal::languageManager()->getCurrentLanguage()->getId();

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $defaults = parent::defaultSettings();
    $defaults['date_date_format'] = 'd/m/Y';
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();
    $element['date_date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date format'),
      '#default_value' => $settings['date_date_format'],
      '#required' => TRUE,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $settings = $this->getSettings();
    $summary[] = $this->t('Date format: %dateformat', [
      '%dateformat' => $settings['date_date_format'],
    ]);
    return $summary;
  }

}
